set nocompatible
filetype off

" Set run-time path to include Vundle, initialize
if has('win32') || has('win64')
	set rtp+=~/vimfiles/bundle/Vundle.vim
else
	set rtp+=~/.vim/bundle/Vundle.vim
endif


call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

Plugin 'Valloric/YouCompleteMe'
Plugin 'jiangmiao/auto-pairs'
Plugin 'vim-scripts/DoxygenToolkit.vim'
" Plugin 'scrooloose/nerdtree.git'
call vundle#end()
filetype plugin indent on


set number
let g:ycm_extra_conf_globlist = ['~/Development/*']
let g:ycm_autoclose_preview_window_after_completion = 1
" autocmd vimenter * NERDTree

" C++ Dev: Auto insert include guards at header creation
function! s:insert_guards()
	let guardname = substitute(toupper(expand("%:t")), "\\.", "_", "g")
	execute "normal! i#ifndef " . guardname
	execute "normal! o#define " . guardname . " "
	execute "normal! o"
	execute "normal! Go#endif // " . guardname 
	normal! kk
	call cursor(3, 1)
endfunction

autocmd BufNewFile *.{h,hpp} call <SID>insert_guards()

" Automatically fix indent in files, restoring position when done.
map <F7> mzgg=G`z

" Set up C comment folding
map <F9> :set foldmethod=marker foldmarker=/*,*/<CR>

" Unmap arrow keys
no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>

